//imageshowing function
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
        $('#image')
        .attr('src', e.target.result)
        .width(150)
        .height(100);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
//datatable function
$(document).ready(function(){
    $('#employee').DataTable();

});
//metis dashboard
$(function() {
    Metis.dashboard();
});
//datepicker
//$( "#datepicker" ).datepicker();
//onclick add employee


$('#addEmployeeContainer').slideUp();
$('#addEmployee').click(function(){
    $('#addEmployeeContainer').slideToggle();
    $(this).text(function(i, text){
          return text === "Add New Employee" ? "Add Later" : "Add New Employee";
      })
})
//form validation
/*
$('form.employeeForm').keyup(function(){
    var that=$(this),
        url=that.attr('action'),
        method=that.attr('method'),
        data={};
    that.find('[name]').each(function(index,value){
        var that=$(this),
            name=that.attr('name'),
            value=that.val();
        data[name]=value;
        if (data[name]=='full_name') {
            console.log(data[name]);
        }

    });
console.log(data);
return false;
});
*/
$('form.employeeForm').on('submit',function(){
      $("span").hide();
  $(".danger").removeClass("danger");
    var that=$(this),
        url=that.attr('action'),
        method=that.attr('method'),
        data={};
    that.find('[name]').each(function(index,value){
        var that=$(this),
            name=that.attr('name'),
            value=that.val();
        data[name]=value;

       
    });
    var form = $('form.employeeForm')[0];
    var formdata=new FormData(form);
    data=formdata.serialize();
     console.log(data);
    // var a = data.serializeArray();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': data['_token']
        }
});

    $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            beforeSend: function () {
                $('#addEmployee').val('Recording data..........');
            },
            complete: function () {
                $('#addEmployee').val('Save Record');
            },
            success: function (data) {
                console.log(data);
            },

           error: function(data){
                var errors= data.responseJSON;
                //var error=errors.name[0];
               $.each(errors.errors, function(key, value) {
                      console.log(key,value);
                      //$('input','textarea').removeClass('error');
                      var dtextarea=$('textarea[name="'+key+'"]').closest('textarea[name="'+key+'"]');
                      dtextarea.next('.text-danger').remove();
                      dtextarea.addClass('danger').after("<span class='text-danger'>"+value+"<br></span>");
                      var dinput=$('input[name="'+key+'"]').closest('input[name="'+key+'"]');
                      dinput.next('.text-danger').remove();
                      dinput.addClass('danger').after("<span class='text-danger'>"+value+"<br></span>");
                      //$('input[name="'+key+'"]').addClass('danger').closest('input[name="'+key+'"]').after("<span class='text-danger'>"+value+"</span>");
                });

                   }
            });  

return false;
});
$('#addtaskpanel').slideUp();
$('#addtaskpanelbutton').click(function(){
  $('#addtaskpanel').slideToggle();
      $(this).text(function(i, text){
          return text === "Assign Task" ? "Assign Later" : "Assign Task";
      });
});

