-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2016 at 10:21 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `otask`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_07_28_191456_create_task_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tasktable`
--

CREATE TABLE `tasktable` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `employee_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `assigner_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `assigner_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `task_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `task_detail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attachments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tasktable`
--

INSERT INTO `tasktable` (`id`, `employee_name`, `employee_id`, `assigner_name`, `assigner_id`, `task_title`, `task_detail`, `attachments`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(7, 'Asif', '123', 'Asif Muztaba', '1', 'Task 1', 'fghf', '1231.10670009_767778989995508_3096387940224598456_n.jpg', '1', NULL, '2016-07-28 15:04:12', '2016-07-28 15:04:12'),
(8, 'Asif Muztaba', '123', 'Asif Muztaba', '1', 'Task 1', 's', '1231.13770417_1046107628790737_4350115597999063303_n.jpg', '1', NULL, '2016-07-28 17:21:33', '2016-07-28 17:21:33'),
(9, 'sdfsdf', '5345', 'Asif Muztaba', '1', 'Task 1', 'iopo', '53451.13495189_851082628359061_4560818318732536395_n.jpg', '1', NULL, '2016-07-28 17:49:06', '2016-07-28 17:49:06'),
(10, 'Asif Muztaba', '1', 'Asif Muztaba', '1', 'Task 1', 'rty', '11.38123660e9b8a16443815313ae632d7e-11.jpg', '1', NULL, '2016-07-28 17:50:07', '2016-07-28 17:50:07'),
(11, 'dsfgdsfg', '345345', 'Asif Muztaba', '1', 'Task 1', 'asd', '3453451.13495189_851082628359061_4560818318732536395_n.jpg', '1', NULL, '2016-07-28 17:53:41', '2016-07-28 17:53:41'),
(12, 'dsfgdsfg', '345345', 'Asif Muztaba', '1', 'Task 1', 'asda', '3453451.38123660e9b8a16443815313ae632d7e-11.jpg', '1', NULL, '2016-07-28 17:56:27', '2016-07-28 17:56:27'),
(13, 'Asif Muztaba', '1', 'Asif Muztaba', '1', 'Task 5', 'Do this', '11.13495189_851082628359061_4560818318732536395_n.jpg', '1', NULL, '2016-07-29 05:49:49', '2016-07-29 05:49:49'),
(14, 'Asif Muztaba', '1', 'dsfgdsfg', '345345', 'do this', 'fggh', '1345345.10670009_767778989995508_3096387940224598456_n.jpg', '1', NULL, '2016-07-29 09:54:58', '2016-07-29 09:54:58');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `join_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birth_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `n_id_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `blood` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `eid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `address`, `phone_no`, `email`, `username`, `password`, `image`, `join_date`, `position`, `status`, `birth_date`, `n_id_no`, `blood`, `eid`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Asif', 'asasas', '123', 'p@gmail.com', 'asif', '$2y$10$yQ.ZgRkdHdHV1mTE9VL4zeDhM6RRnjZ94jwbw5397y4a8m.5Wm/6e', 'asif.13236065_257790284574577_49725600_n.jpg', '26-06-2016', 'Content Creator', '1', '26-06-2016', '123', 'B+', '123', NULL, '2016-06-04 15:32:11', '2016-06-04 15:32:11'),
(2, 'sdfsdf', 'dfgsdfgdsfg', '45645654645656', 'amuztaba18@gmail.com', 'sdfsdf', '$2y$10$5Bz2zQr6Mfi42wr9MbwqaejNKPwUDVhPtt0GmtukrkOTAjA4Oa4eq', 'sdfsdf.10670009_767778989995508_3096387940224598456_n.jpg', '06-07-2016', 'Full Stack Engineer', '1', '01-09-2015', '2345635634565464563456', 'A+', '5345', NULL, '2016-07-27 09:20:52', '2016-07-27 09:20:52'),
(3, 'dsfgdsfg', 'dfgdfgh', '345345', 'a@a.com', 'sdfgdsfg', '$2y$10$woHifzK8f.lMxRc3kBjySeCa5nI59EkGLYwaQUbIM0k3B/pFveWgy', 'sdfgdsfg.13770417_1046107628790737_4350115597999063303_n.jpg', '04-07-2016', 'Web Application Developer', '1', '25-07-2016', '3453456456467467', 'A+', '345345', 'MyYUFpsDkCQX81aT8eFuM99ov0xrPEyPbrgEfL0rKdot0ggwnOe1c46MUN8r', '2016-07-28 01:36:40', '2016-07-28 11:55:13'),
(4, 'ertertety', 'gdfgsdfgh', '6436456', 'a@w.com', 'tryrtyrty', '$2y$10$eKigc66MY3owsHTZ/RNlH.Wl8DvKSwc0IG2ysqhnwniJsozeB3YCe', 'tryrtyrty.13495189_851082628359061_4560818318732536395_n.jpg', '05-07-2016', 'Web Application Developer', '1', '11-07-2016', '45645656356456456', 'A+', '45345', NULL, '2016-07-28 01:38:17', '2016-07-28 01:38:17'),
(5, 'Asif Muztaba', 'pabna', '123456789', 'asifmuztaba18@gmail.com', 'asifnew', '$2y$10$tlh75wggAMKTpnEAkN1Z9eUUNsgvUxzwmiYqc3.pqlmV6h0sQLLoS', 'asifnew.p.jpg', '01-07-2016', 'Full Stack Engineer', '1', '14-08-1993', '123456789', 'A+', '1', '4xnTGOw9LkuFeahXZB3ln6n5ubvhTxbTKXaGV8rdpgEkX98mr1ZABvsHsr6J', '2016-07-28 11:54:51', '2016-07-28 11:55:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `tasktable`
--
ALTER TABLE `tasktable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tasktable`
--
ALTER TABLE `tasktable`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
