<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function()
{
    if (Auth::check()){ return Redirect::to('/home');}
    else{
    	return Redirect::to('/login');
    }

});
Route::group(['middleware' => 'web'], function () {
Route::auth();
Route::get('/home', 'HomeController@index');
Route::any('/employees', 'EmployeeController@index');
Route::any('/addtask', 'EmployeeController@addtask');
Route::any('/assignupload', 'EmployeeController@assignupload');
Route::any('/getnotification', 'Notifycontroller@index');
Route::any('/getnotificationcontent', 'Notifycontroller@getnotificationcontent');
Route::any('/notificationdetails/{id}', 'Notifycontroller@notificationdetails');
Route::post('/registration', 'EmployeeController@eregister');
 });