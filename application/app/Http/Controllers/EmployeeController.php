<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Taskmodel;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

class EmployeeController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }
  public function index(){
    $getdata = User::where('status', 1)->get();

    return view('employee')->with('getData', $getdata);
  }
  public function eregister(Request $request){
    $getdata = User::where('status', 1)->get();
    if ($request->isMethod('POST')) {
      $rules = [

      'full_name' => 'required',
      'image' => 'required',
      'eid' => 'required',
      'username' => 'required',
      'address' => 'required',
      'join_date' => 'required',
      'n_id_no' => 'required',
      'position' => 'required',
      'blood' => 'required',
      'birth_date' => 'required',
                  // 'role' => 'required',
      'phone_no' => 'required',
      'email' => 'required|email|max:255|unique:users',
      'password' => 'required|confirmed',
      'password_confirmation' => 'required|min:6',
      ];
      $validation = Validator::make($request->all(), $rules);
      $errors=$validation->errors();
      print_r($errors);
      if ($validation->fails()) {
              	//return response()->json([
								     //       'status' => 'false',
								      //      'errors'  =>$validation->errors()], 400);
                //return response()->json($validation->errors());
        Session::flash('flash_message', 'Please Check Again.');
        Session::flash('flash_type', 'alert-danger');
        return redirect()->back()->with('getData', $getdata)->withErrors($validation->errors())->withinput($request->all());
      } else {
        $uploadDirectory = 'uploads/';
        $file = $request->file('image');
        $filename = $request->input('username') . "." . $file->getClientOriginalName();
        $saveData = new User();
        if ($file->move($uploadDirectory, $filename)) {
          $saveData->image = $filename;
          $saveData->position = $request->input('position');
          $saveData->full_name = $request->input('full_name');
          $saveData->eid = $request->input('eid');
          $saveData->phone_no = $request->input('phone_no');
          $saveData->username = $request->input('username');
          $saveData->address = $request->input('address');
          $saveData->join_date = $request->input('join_date');
          $saveData->n_id_no = $request->input('n_id_no');

          $saveData->email = $request->input('email');
          $saveData->blood = $request->input('blood');
          $saveData->birth_date = $request->input('birth_date');
          $saveData->status =1;
          $saveData->password = Hash::make($request->input('password'));
          if($saveData->save()){
            Session::flash('flash_message', 'Successfully registered.');
            Session::flash('flash_type', 'alert-success');
            return redirect('employees');
          }
        }
      }
    } else {  
      Session::flash('flash_message', 'Not registered.');
      Session::flash('flash_type', 'alert-danger');
      return view('employee')->with('getData', $getdata);
    }
  }
  public function addtask(){
    $getdata = User::where('status', 1)->get();
    return view('taskboard')->with('getData', $getdata);
  }
  public function assignupload(Request $request){

    $getdata = User::where('status', 1)->get();
        if ($request->isMethod('POST')) {
          $employee_ids=$request->input('employee_id');
          $employee_names=User::where('eid',$employee_ids)->first();
          $assigner_names=Auth::user()->full_name;
          $assigner_ids=Auth::user()->eid;

$employee_name_single=$employee_names->full_name;
$employee_id_single=$employee_ids;
$assigner_name_single=$assigner_names;
$assigner_id_single=$assigner_ids;
         
          //print_r($employee_id_single);
          //exit();
      $rules = [

      'employee_id' => 'required',

      'task_title' => 'required',
      'task_detail' => 'required',
      'attachments' => 'required',
      ];
      $validation = Validator::make($request->all(), $rules);
      $errors=$validation->errors();

      if ($validation->fails()) {
                //return response()->json([
                     //       'status' => 'false',
                      //      'errors'  =>$validation->errors()], 400);
                //return response()->json($validation->errors());
        Session::flash('flash_message', 'Please Check Again.');
        Session::flash('flash_type', 'alert-danger');
        return view('taskboard')->with('getData', $getdata)->withErrors($validation->errors())->withinput($request->all());
      } else {
        $data=$request->all();
       // print_r($data);
        //print_r($employee_id_single);
        //print_r($assigner_id_single);
        //exit();
        $uploadDirectory = 'uploads/';
        $file = $request->file('attachments');
        $filename = $employee_id_single .$assigner_id_single. "." . $file->getClientOriginalName();
        $saveData = new Taskmodel();
        if ($file->move($uploadDirectory, $filename)) {
          $saveData->attachments = $filename;
          $saveData->employee_name = $employee_name_single;
          $saveData->employee_id = $employee_id_single;
          $saveData->assigner_name = $assigner_name_single;
          $saveData->assigner_id = $assigner_id_single;
          $saveData->task_title = $request->input('task_title');
          $saveData->task_detail = $request->input('task_detail');
          $saveData->status =1;
          if($saveData->save()){
            Session::flash('flash_message', 'Successfully assigned.');
            Session::flash('flash_type', 'alert-success');
            return redirect('addtask');
          }
        }
      }
    } else {  
      Session::flash('flash_message', 'Not Assigned.');
      Session::flash('flash_type', 'alert-danger');
      return view('taskboard')->with('getData', $getdata);
    }
    return view('taskboard')->with('getData', $getdata);
  }   
}
