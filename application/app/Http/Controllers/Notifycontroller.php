<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Taskmodel;
class Notifycontroller extends Controller
{
     public function __construct()
  {
    $this->middleware('auth');
  }
    public function index(Request $request){
    $ids=$request->input('id');
$current = Carbon::now();
$dt=$current->subSecond(2);
$data=Taskmodel::where('employee_id', $ids)
					->where('status',1)
					->count();

    return $data;
  }
  public function getnotificationcontent(Request $request){
  	    $ids=$request->input('id');
  	    $basePath=public_path();
  	    $data=Taskmodel::where('employee_id', $ids)
					->where('status',1)
					->get();
					$datacode=array();
					$key=1;
					foreach ($data as $datam) {
						$datacode[$key]="<div class='notificationHolderGroup'><a href='notificationdetails/".$datam->id."' style='color:#fff;text-decoration:none;'><p>".$datam->task_title."<span style='margin-left:65px;'>".$datam->created_at."</span></p></a><span>".$datam->assigner_name."</span></div>";
                  $key++;
					}
				
						return $datacode;
					
								
  }
  public function notificationdetails($id){
$ids=$id;
return $ids;
  }
}
