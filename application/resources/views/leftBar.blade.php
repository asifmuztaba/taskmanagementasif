      <div id="left">
        <div class="media user-media bg-dark dker">
          <div class="user-media-toggleHover">
            <span class="fa fa-user"></span> 
          </div>
          <div class="user-wrapper bg-dark">
            <a class="user-link" href="">
              <img style="    width: 100px; height:100px;" class="media-object img-thumbnail user-img" alt="User Picture" src="{{ asset('uploads/'.Auth::user()->image)}}">
              <span class="label label-danger user-label">16</span> 
            </a> 
            <div class="media-body">
              <h5 class="media-heading">{{Auth::user()->full_name}}</h5>
              <ul class="list-unstyled user-info">
                <li> <a href=""></a>{{Auth::user()->position}}  </li>
              </ul>
            </div>
          </div>
        </div>

        <!-- #menu -->
        <ul id="menu" class="bg-blue dker">
          <li class="nav-header">Menu</li>
          <li class="nav-divider"></li>
          <li class="">
            <a href="{{ URL::to('dashboard') }}">
              <i class="fa fa-dashboard"></i><span class="link-title">&nbsp;Dashboard</span> 
            </a> 
          </li>
          <li class="">
            <a href="{{ URL::to('employees') }}">
              <i class="fa fa-user"></i><span class="link-title">&nbsp;Employee Section</span> 
            </a> 
          </li>

          <li class="">
            <a href="{{ URL::to('addtask') }}">
              <i class="fa fa-user"></i><span class="link-title">&nbsp;Add Task</span> 
            </a> 
          </li>
          <li class="">
            <a href="javascript:;">
              <i class="fa fa-building "></i>
              <span class="link-title">Employee Section</span> 
              <span class="fa arrow"></span> 
            </a> 
            <ul>
              <li>
                <a href="boxed.html">
                  <i class="fa fa-angle-right"></i>&nbsp; Boxed Layout </a> 
              </li>
            </ul>
          </li>
        </ul><!-- /#menu -->
      </div><!-- /#left -->
      <div id="content">
        <div class="outer">
          <div class="inner bg-light lter">