@extends('master')
@section('content')
<div class="row">
	<div class="employee-header">
		<button class="btn btn-primary" id="addEmployee">Add New Employee</button>
	</div>
	<hr>
	<div class="addEmployee" id="addEmployeeContainer">
	        <form action="{{url('/registration')}}" method="post"  enctype="multipart/form-data" class="employeeForm">
            {!! csrf_field() !!}
            <legend></legend>
            <table class="table table-striped table-bordered">
                <tr>
                    <th>Image<i class="fa fa-asterisk required" aria-hidden="true"></i></th>
                    <th >
                        <input type="file" name="image" class="form-control" onchange="readURL(this);">
                         @if ($errors->has('image'))
                        }
                        }
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('image') }}</strong>
                        </span>
                        @endif
 
                    </th>
                    <th style="vertical-align: middle;" rowspan="2">Image</th>
                    <td rowspan="2">
                        <img id="image" src="" alt="your selected image will be shown here" />
                        
                    </td>
                </tr>
                <tr>
                	<th>Employee ID:<i class="fa fa-asterisk required" aria-hidden="true"></i></th>
                	<th>
                	<input type="text" name="eid" class="form-control">
                	@if ($errors->has('eid'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('eid') }}</strong>
                        </span>
                    @endif
                    </th>
                </tr>
                <tr>
                    <th>Full Name<i class="fa fa-asterisk required" aria-hidden="true"></i></th>
                    <th>
                        <input type="text" name="full_name" class="form-control">
                         @if ($errors->has('full_name'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('full_name') }}</strong>
                        </span>
                        @endif
                    </th>
                    <th>Username<i class="fa fa-asterisk required" aria-hidden="true"></i></th>
                    <th>
                        <input type="text" name="username" class="form-control">
                         @if ($errors->has('username'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('username') }}</strong>
                        </span>
                        @endif
                    </th>
                </tr>
                <tr>
                    <th >Email<i class="fa fa-asterisk required" aria-hidden="true"></i></th>
                    <th>
                        <input type="text" name="email" class="form-control">
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </th>
                    <th>Phone<i class="fa fa-asterisk required" aria-hidden="true"></i></th>
                    <th>
                        <input type="text" name="phone_no" class="form-control">
                        
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('phone_no') }}</strong>
                        </span>
                      
                    </th>
                </tr>
                <tr>
                    <th style="vertical-align: middle;">Address<i class="fa fa-asterisk required" aria-hidden="true"></i></th>
                    <th>
                        <textarea name="address" class="form-control"></textarea>
                    </th>
                    <th style="vertical-align: middle;">Joining Date<i class="fa fa-asterisk required" aria-hidden="true"></i></th>
                    <th>
                        <input name="join_date" id="" class="form-control datepicker">
                    </th>
                </tr>
                <tr>
                    <th style="vertical-align: middle">N Id No<i class="fa fa-asterisk required" aria-hidden="true"></i></th>
                    <th>
                        <input type="text" name="n_id_no" class="form-control">
                    </th>
                    <th style="vertical-align: middle">Designation<i class="fa fa-asterisk required" aria-hidden="true"></i></th>
                    <th>
                        <select name="position" class="form-control" required="">
                            <option value="">Select</option>
                            <option value="Full Stack Engineer">Full Stack Engineer</option>
                            <option value="Web Development Engineer">Web Development Engineer</option>
                            <option value="Mobile Apps Developer">Mobile Apps Developer</option>
                            <option value="Web Application Developer">Web Application Developer</option>
                            <option value="Web Developer">Web Developer</option>
                            <option value="Graphics Designer">Graphics Designer</option>
                            <option value="Content Creator">Content Creator</option>
                        </select>
                    </th>
                </tr>
                <tr>
                    <th>Password<i class="fa fa-asterisk required" aria-hidden="true"></i></th>
                    <th>
                        <input type="password" name="password" class="form-control">
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                       
                    </th>
                    <th>Confirmed Password</th>
                    <th>
                        <input type="password" class="form-control" name="password_confirmation">
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </th>
                </tr>
                <tr>
                    <th>Blood Group</th>
                    <th>
                        <select name="blood" class="form-control" required="">
                            <option value="">Select</option>
                            <option value="A+">A+</option>
                            <option value="B+">B+</option>
                            <option value="O+">O+</option>
                            <option value="AB+">AB+</option>
                            <option value="A-">A-</option>
                            <option value="B-">B-</option>
                            <option value="O-">O-</option>
                            
                            <option value="AB-">AB-</option>
                        </select>
                        </th>
                    <th style="vertical-align: middle;">Birth Date<i class="fa fa-asterisk required" aria-hidden="true"></i></th>
                    <th>
                        <input name="birth_date" id="" class="form-control datepicker">
                    </th>
                </tr>
                <tr>
                    <th colspan="4">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-sign-in fa-fw"></i>Registration</button>
                    </th>
                </tr>
            </table>
        </form>
      </div>
	
	<div class="list-header"><span>All Current Employee List</span></div>
	<div class="list-box">
<table id="employee" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Start date</th>

                <th>More</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Start date</th>
                <th>More</th>
            </tr>
        </tfoot>
        <tbody>
        @if(!empty($getData))
            @foreach($getData as $data)
            <tr>
                <td>{{$data->full_name}}</td>
                <td>{{$data->position}}</td>
                <td>{{$data->join_date}}</td>
                <td><a href="" class="btn btn-primary">view details</a> <a href="" class="btn btn-danger">delete</a></td>
            </tr>
            @endforeach
            @endif

        </tbody>
    </table>
	</div>
</div>
@stop
@section('modal')
<!-- #helpModal -->
    <div id="employeereg" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Registration Form</h4>
          </div>
          <div class="modal-body">

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal --><!-- /#helpModal -->
    @stop