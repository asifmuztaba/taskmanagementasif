<!doctype html>
<html class="no-js">
  <head>
    <meta charset="UTF-8">
    <title>OTask</title>

    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/datatable.min.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,300italic,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

    <!-- metisMenu stylesheet -->
    <link rel="stylesheet" href="{{ asset('css/metisMenu.min.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>
      <script src="assets/lib/html5shiv/html5shiv.js"></script>
      <script src="assets/lib/respond/respond.min.js"></script>
      <![endif]-->

    <!--For Development Only. Not required -->
    
    <link rel="stylesheet" href="{{ asset('css/style-switcher.css') }}">
    
    <script src="{{ asset('js/less.min.js')}}"></script>

    <!--Modernizr-->
    <script src="{{ asset('js/modernizr.min.js') }}"></script>
  </head>
  <body class="  ">
<div id="loading"><img src="{{ asset('img/ajax-loader.gif') }}">
</div>
<div id="page">
    <div class="bg-dark dk" id="wrap">