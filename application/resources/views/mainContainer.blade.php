
      <div id="content">
        <div class="outer">
          <div class="inner bg-light lter">
            <div class="col-lg-12">
              <h1 id="bootstrap-admin-template">Bootstrap-Admin-Template</h1>
              <p>Metis is a simple yet powerful free Bootstrap admin dashboard template that you can feel free to use for any app, SaaS, admin dashboard, user dashbboard, web app, service, software or anything else. Feel free to share and fork it.</p>
              <p>This template currently is slighly outdated but withing few weeks we are going to make a major overhaul making ot the best free admin template you have seen on Github or elsehwere on the web.</p>
              <ul>
                <li>
                  <a href="https://travis-ci.org/puikinsh/Bootstrap-Admin-Template">
                    <img src="https://travis-ci.org/puikinsh/Bootstrap-Admin-Template.svg" alt="Build Status">
                  </a> 
                </li>
                <li>
                  <a href="https://david-dm.org/puikinsh/Bootstrap-Admin-Template">
                    <img src="https://david-dm.org/puikinsh/Bootstrap-Admin-Template.svg?theme=shields.io" alt="Dependency Status">
                  </a> 
                </li>
                <li>
                  <a href="https://david-dm.org/puikinsh/Bootstrap-Admin-Template#info=devDependencies">
                    <img src="https://david-dm.org/puikinsh/Bootstrap-Admin-Template/dev-status.svg?theme=shields.io" alt="devDependency Status">
                  </a> 
                </li>
                <li>
                  <a href="http://gruntjs.com/">
                    <img src="https://cdn.gruntjs.com/builtwith.png" alt="Built with Grunt">
                  </a> 
                </li>
              </ul>
              <h2 id="toc">TOC</h2>
              <ul>
                <li> <a href="#download">Download</a>  </li>
                <li> <a href="#building">Building</a>  </li>
                <li> <a href="#demo">Demo</a>  </li>
                <li> <a href="#other">Other</a>  </li>
                <li> <a href="#credits">Credits</a>  </li>
                <li> <a href="#author">Author</a>  </li>
                <li> <a href="#license">License</a>  </li>
              </ul>
              <h2 id="download">Download</h2>
              <ul>
                <li>
                  <p>Bootstrap 2.3.2
                    <a href="https://github.com/puikinsh/Bootstrap-Admin-Template/archive/v1.2.zip"> v1.2</a> 
                    ready for use</p>
                </li>
                <li>
                  <p>Bootstrap 3.3.0
                    <a href="https://puikinsh.com/onokumus/Bootstrap-Admin-Template/archive/master.zip"> v2.3.2</a> 
                    ready <code>dist</code> folder your use</p>
                </li>
              </ul>
              <h2 id="building">Building</h2>
              <h4 id="2-3-2-version">2.3.2 Version</h4>
              <p>required
                <a href="http://nodejs.org/">node.js</a>  &amp;
                <a href="http://bower.io/">bower</a>  &amp;
                <a href="http://gruntjs.com/getting-started">grunt</a> 
              </p>
              <pre><code class="language-shell">    $ git clone https://github.com/puikinsh/Bootstrap-Admin-Template.git yourfoldername
    $ cd yourfoldername
    $ npm install
    $ bower install
    $ npm run build
    $ grunt serve</code></pre>
              <h4 id="2-3-2-rtl-version">2.3.2 RTL Version</h4>
              <p>required
                <a href="http://nodejs.org/">node.js</a>  &amp;
                <a href="http://bower.io/">bower</a>  &amp;
                <a href="http://gruntjs.com/getting-started">grunt</a> 
              </p>
              <pre><code class="language-shell">    $ git clone https://github.com/puikinsh/Bootstrap-Admin-Template.git yourfoldername
    $ cd yourfoldername
    $ npm install
    $ bower install
    $ npm run buildrtl
    $ grunt serve</code></pre>
              <h4 id="1-2-version">1.2 Version</h4>
              <pre><code class="language-shell">    $ git clone -b v1.2 https://github.com/puikinsh/Bootstrap-Admin-Template.git yourfoldername
    $ cd yourfoldername
    $ git submodule init
    $ git submodule update
    $ open index.html</code></pre>
              <h2 id="demo">Demo</h2>
              <ul>
                <li> <a href="https://colorlib.com/polygon/metis/">Demo v2.3.2</a>  </li>
                <li> <a href="https://colorlib.com/polygon/metis/rtl/">RTL v2.3.2</a>  </li>
                <li></li>
              </ul>
              <h2 id="other-templates-and-useful-resources">Other templates and useful resources</h2>
              <ul>
                <li>
                  <p>
                    <a href="https://colorlib.com/wp/free-bootstrap-admin-dashboard-templates/" title="Free Bootstrap Admin Templates on Colorlib">Free Admin Templates</a>  - A comprehensive list of the best free Bootstrap admin dashboard templates. All templates are built based on Bootstrap frontend framework making them responsive and mobile friendly. Most of them can compete
                    with premium admin templates. </p>
                </li>
                <li>
                  <p>
                    <a href="https://colorlib.com/wp/free-html5-admin-dashboard-templates/" title="List of free HTML based admin templates by Colorlib">Free Admin Dashboard Templates</a>  - Versatile list of the best free HTML5/CSS3 powered admin templates. Templates are licensed under MIT license making them free for personal and commercial use. However, no support is provided with
                    these templates. </p>
                </li>
                <li>
                  <p>
                    <a href="https://colorlib.com/wp/angularjs-admin-templates/" title="Angular Admin Templates on Colorlib">AngularJS Templates</a>  - Comprehensive list of the most popular AngularJS admin templates. Most of these templates comes with both HTML and Angular version therefore they are not just strictly limited to Angular or some other platform.
                    </p>
                </li>
                <li>
                  <p>
                    <a href="https://colorlib.com/wp/html-admin-templates/" title="Premium HTML Material Design Admin Templates on Colorlib">Premium Admin Templates</a>  - Comprehensive list of premium admin templates designed uing Flat and Material design. Most of these templates comes with both AngularJS and HTML version making your universal tool.</p>
                </li>
                <li>
                  <p>
                    <a href="https://colorlib.com/wp/bootstrap-admin-templates/" title="List of Premium Bootstrap Admin Templates by Colorlib">Bootstrap Admin Templates</a>  - List of Bootstrap admin dashboard templates that uses a minimal yet stunning Flat or Material design. Most of these templates comes as HTML and AngularJS version making them universal and easy to use
                    for your upcoming SaaS, web application, user dashboard, admin dashboard, CRM, ERP, database, or any other data driven interface. </p>
                </li>
                <li>
                  <p>
                    <a href="https://colorlib.com/wp/wordpress-admin-dashboard-themes-plugins/" title="List of WordPress Admin Dashboard Templates and Plugins by Colorlib">WordPress Admin Templates</a>  - List of stunning and customizable WordPress admin dashboard templates that will add a personal touch to your WordPress dashboard via template or plugin. </p>
                </li>
                <li>
                  <p>
                    <a href="https://colorlib.com/wp/free-wordpress-themes/" title="List of Free WordPress themes by Colorlib">WordPress Themes</a>  - Most versatile list of free WordPress themes all licensed under GPL. These themes can be used, shared, tweaked, redistributed without restriction as long as you keep the original copyright/license information.</p>
                </li>
              </ul>
              <h2 id="credits">Credits</h2>
              <ul>
                <li> <a href="http://nodejs.org/">node.js</a>  </li>
                <li> <a href="http://bower.io/">bower</a>  </li>
                <li> <a href="http://gruntjs.com/">Grunt</a>  </li>
                <li> <a href="http://assemble.io/">Assemble</a>  </li>
                <li> <a href="http://jquery.com/">jQuery</a>  </li>
                <li> <a href="http://getbootstrap.com/">Bootstrap</a>  </li>
                <li> <a href="http://lesscss.org/">LESS</a>  </li>
                <li> <a href="http://modernizr.com/">Modernizr</a>  </li>
                <li> <a href="http://momentjs.com/">Moment.js</a>  </li>
                <li> <a href="https://github.com/subtlepatterns/SubtlePatterns">SubtlePatterns</a>  </li>
                <li> <a href="http://arshaw.com/fullcalendar/">FullCalendar</a>  </li>
                <li> <a href="https://github.com/harvesthq/chosen">Chosen</a>  </li>
                <li> <a href="http://ckeditor.com/">CKEditor</a>  </li>
                <li> <a href="http://www.eyecon.ro/bootstrap-colorpicker/">Colorpicker for Bootstrap</a>  </li>
                <li> <a href="http://www.datatables.net">Data Tables</a>  </li>
                <li> <a href="http://www.eyecon.ro/bootstrap-datepicker">Datepicker for Bootstrap</a>  </li>
                <li> <a href="http://elfinder.org">elFinder</a>  </li>
                <li> <a href="http://rustyjeans.com/jquery-plugins/input-limiter">Input Limiter</a>  </li>
                <li> <a href="http://jasny.github.com/bootstrap">Jasny Bootstrap</a>  </li>
                <li> <a href="http://jqueryvalidation.org/">jQuery Validation</a>  </li>
                <li> <a href="http://omnipotent.net/jquery.sparkline">jQuery Sparklines</a>  </li>
                <li> <a href="http://daneden.github.io/animate.css/">Animate</a>  </li>
                <li> <a href="http://www.jacklmoore.com/autosize">Autosize</a>  </li>
                <li> <a href="http://keith-wood.name/countdown.html">Countdown</a>  </li>
                <li> <a href="https://github.com/dangrossman/bootstrap-daterangepicker">Date range picker</a>  </li>
                <li> <a href="http://www.flotcharts.org">Flot</a>  </li>
                <li> <a href="http://jquery.malsup.com/form/">jQuery Form</a>  </li>
                <li> <a href="http://thecodemine.org">Form Wizard</a>  </li>
                <li> <a href="http://boedesign.com/blog/2009/07/11/growl-for-jquery-gritter/">Gritter</a>  </li>
                <li> <a href="https://github.com/brandonaaron/jquery-mousewheel">Mouse Wheel</a>  </li>
                <li> <a href="https://github.com/kevinoconnor7/pagedown-bootstrap">PageDown-Bootstrap</a>  </li>
                <li> <a href="https://github.com/moxiecode/plupload">Plupload</a>  </li>
                <li> <a href="http://www.larentis.eu/switch/">Bootstrap Switch</a>  </li>
                <li> <a href="http://tablesorter.com/">tablesorter</a>  </li>
                <li> <a href="http://xoxco.com/projects/code/tagsinput/">tagsinput</a>  </li>
                <li> <a href="http://jdewit.github.io/bootstrap-timepicker/">Bootstrap Timepicker</a>  </li>
                <li> <a href="http://touchpunch.furf.com/">Touch Punch</a>  </li>
                <li> <a href="http://uniformjs.com/">Uniform</a>  </li>
                <li> <a href="http://www.position-relative.net/">Validation Engine</a>  </li>
                <li> <a href="http://validval.frebsite.nl/">jquery.validVal</a>  </li>
                <li> <a href="https://github.com/Waxolunist/bootstrap3-wysihtml5-bower">bootstrap3-wysihtml5-bower</a>  </li>
                <li> <a href="https://github.com/sindresorhus/screenfull.js">screenfull.js</a>  </li>
                <li> <a href="https://github.com/onokumus/metisMenu">metisMenu</a>  </li>
              </ul>
              <h2 id="author">Author</h2>
              <ul>
                <li> <a href="https://colorlib.com/">Colorlib</a>  - Colorlib is the most popular source for free WordPress themes and HTML templates. </li>
                <li> <a href="https://twitter.com/AigarsSilkalns">Aigars Silkalns</a>  - Aigars maintains this project and is also the idea author behind Colorlib and everythiwn you will find on that website. </li>
              </ul>
              <h2 id="license">License</h2>
              <p>Copyright (c) 2016 Aigars Silkalns &amp; Colorlib</p>
              <p>Released under the MIT license. This free Bootstrap admin template is distributed as as it with no support. You can feeel free to use it, share it, tweakit it, workin it, sell it or do whatver you want as long as you keep the original license
                in plce.</p>
              <hr>
              <p><em>This file was generated by <a href="https://github.com/assemble/verb-cli">verb-cli</a>  on March 05, 2016.</em> </p>
            </div>
          </div><!-- /.inner -->
        </div><!-- /.outer -->
      </div><!-- /#content -->