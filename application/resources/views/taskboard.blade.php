@extends('master')
@section('content')
<div class="row">
	<div class="toptaskmenu">
        <button id="addtaskpanelbutton" class="btn btn-success">Assign Task</button>  
        <a href=""><button class="btn btn-info">View Previous</button></a>  
        <a href=""><button class="btn btn-warning">Send Massege</button></a>   
    </div>
    <div class="addtaskpanel" id="addtaskpanel">
        <form class="form-horizontal" role="form" method="post" action="{{ url('/assignupload') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('employee_id') ? ' has-error' : '' }}">
                <label for="employee_id" class="col-md-4 control-label">Employee Name</label>

                <div class="col-md-6">
                    @if(!empty($getData))
                    @foreach($getData as $data)
                    <div class="row">
                        <div class="col-md-4">
                            <input type="radio" name="employee_id" value="{{$data->eid}}">{{$data->full_name}}</input>
                            <br>
                            <span>{{$data->position}}</span>
                        </div>
                        <div class="col-md-3">
                            <img style="    width: 75px; height:75px; " class="media-object img-thumbnail user-img" alt="User Picture" src="{{ asset('uploads/'.$data->image)}}">
                        </div>
                    </div>
                    @endforeach
                    @endif
                    @if ($errors->has('employee_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('employee_id') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('task_title') ? ' has-error' : '' }}">
                <label for="task_title" class="col-md-4 control-label">Task Title</label>

                <div class="col-md-6">
                    <input id="task_title" type="text" class="form-control" name="task_title" value=""></input>

                    @if ($errors->has('task_title'))
                    <span class="help-block">
                        <strong>{{ $errors->first('task_title') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('task_detail') ? ' has-error' : '' }}">
                <label for="task_detail" class="col-md-4 control-label">Task in Detail</label>

                <div class="col-md-6">
                    <textarea id="task_detail" type="text" class="form-control" name="task_detail" value=""></textarea>

                    @if ($errors->has('task_detail'))
                    <span class="help-block">
                        <strong>{{ $errors->first('task_detail') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('attachments') ? ' has-error' : '' }}">
                <label for="attachments" class="col-md-4 control-label">Attachments</label>

                <div class="col-md-6">
                    <input id="attachments" type="file" class="form-control" name="attachments">

                    @if ($errors->has('attachments'))
                    <span class="help-block">
                        <strong>{{ $errors->first('attachments') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i> Assign
                    </button>
                </div>
            </div>
        </form>        
    </div>
    <div class="taskstatistic">
      <ul class="stats_box">
        <li>
          <div class="sparkline bar_week"></div>
          <div class="stat_text">
            <strong>2.345</strong> Weekly Visit
            <span class="percent down"> <i class="fa fa-caret-down"></i> -16%</span> 
        </div>
    </li>
    <li>
      <div class="sparkline line_day"></div>
      <div class="stat_text">
        <strong>165</strong> Daily Visit
        <span class="percent up"> <i class="fa fa-caret-up"></i> +23%</span> 
    </div>
</li>
<li>
  <div class="sparkline pie_week"></div>
  <div class="stat_text">
    <strong>$2 345.00</strong> Weekly Sale
    <span class="percent"> 0%</span> 
</div>
</li>
<li>
  <div class="sparkline stacked_month"></div>
  <div class="stat_text">
    <strong>$678.00</strong> Monthly Sale
    <span class="percent down"> <i class="fa fa-caret-down"></i> -10%</span> 
</div>
</li>
</ul>
</div>
</div>
@stop
@section('modal')
<!-- #helpModal -->
<div id="employeereg" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Registration Form</h4>
    </div>
    <div class="modal-body">

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal --><!-- /#helpModal -->
@stop