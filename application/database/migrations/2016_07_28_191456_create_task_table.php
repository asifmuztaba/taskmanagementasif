<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasktable', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_name');
            $table->string('employee_id');
            $table->string('assigner_name');
            $table->string('assigner_id');
            $table->string('task_title');
            $table->string('task_detail');
            $table->string('attachments');
            $table->string('status');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
